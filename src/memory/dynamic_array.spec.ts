import MemoryManagement from './memory_management';

const MEMORY_SIZE = 1000;

/*
* The purpose of these tests is not to check whether the methods work PROPERLY. The aim
* is to test if the DynamicArray Proxy work with the Array methods.
*/

describe('Dynamic Array', () => {
    let array;
    let memory;
    beforeEach(() => {
        memory = new MemoryManagement(MEMORY_SIZE);
        array = memory.newArray(10);
    });

    describe('Get operator', () => {
        test('Existing element', () => {
            expect(array[0]).toBeDefined();
        });

        test('Non-existing element', () => {
            expect(array[10]).not.toBeDefined();
        });
    });

    describe('Set operator', () => {
        test('Existing element', () => {
            expect(array[0] = 1).toEqual(1);
        });

        test('Non-existing element', () => {
            try {
                array[10] = 1;
            } catch (e) {
                expect(e).toBeInstanceOf(TypeError);
            }
        });
    });

    test('Access existing element after deletion', () => {
        memory.freeArray(array);
        expect(array[0]).toBeUndefined();
    });

    describe('Array prototype methods', () => {
        test('Is array', () => {
            expect(Array.isArray(array)).toBe(true);
        });

        test('Is iterable', () => {
            expect(typeof array[Symbol.iterator]).toBe('function');
        });

        test('Every', () => {
            expect(array.every(element => element === 0)).toBe(true);
        });

        test('Filter', () => {
            expect(array.filter(element => element === 0)).toEqual(new Array(10).fill(0));
        });

        test('Find', () => {
             expect(array.find(element => element !== 0)).not.toBeDefined();
        });

        test('Find index', () => {
            expect(array.findIndex(element => element === 0)).toBe(0);
        });

        test('Fill', () => {
            array.fill(1);
            expect(array).toEqual(new Array(10).fill(1));
        });

        test('Index of', () => {
            array[8] = 1;
            expect(array.indexOf(1)).toBe(8);
        });

        test('Includes', () => {
            expect(array.includes(1)).toBe(false);
        });

        test('Last index of', () => {
            array[2] = 1;
            array[8] = 1;
            expect(array.lastIndexOf(1)).toBe(8);
        });

        test('Reverse', () => {
            array[0] = 1;
            array[9] = 2;
            const newArray = new Array(10).fill(0);
            newArray[0] = 2;
            newArray[9] = 1;
            expect(array.reverse()).toEqual(newArray);
        });

        test('Some', () => {
            expect(array.some(element => element > 0)).toBe(false);
        });

        test('Sort', () => {
            array[5] = 2;
            const newArray = new Array(10).fill(0);
            newArray[9] = 2;
            expect(array.sort()).toEqual(newArray);
        });

        test('Value of', () => {
            expect(array.valueOf()).toEqual(array);
        });

        test('Map', () => {
            expect(array.map(element => element + 1)).toEqual(new Array(10).fill(1));
        });
    });
});
