import MemoryManagement, { EMPTY_ARRAY_ID, IMemoryManagement } from './memory_management';

describe('Memory Management', () => {
    let memory: IMemoryManagement;
    const MEMORY_SIZE = 1000;

    beforeEach(() => {
        memory = new MemoryManagement(MEMORY_SIZE);
    });

    test('Create new Memory Management', () => {
        expect(memory).toBeDefined();
        expect(memory).toHaveProperty('newArray');
        expect(memory).toHaveProperty('resizeArray');
        expect(memory).toHaveProperty('freeArray');

        expect(memory).toHaveProperty('memory', new Int32Array(MEMORY_SIZE));
        expect(memory).toHaveProperty('arrayData', [{
            id: EMPTY_ARRAY_ID,
            index: 0,
            startIndex: 0,
            endIndex: MEMORY_SIZE
        }]);
        expect(memory).toHaveProperty('data', {
            [EMPTY_ARRAY_ID]: 0
        });
        expect(memory).toHaveProperty('freeSize', MEMORY_SIZE);
    });

    test('Create new array', () => {
        const array = memory.newArray(10);

        expect(array).not.toBeNull();
        expect(array).toHaveProperty('length', 10);

        expect(getArrayIndex(array['id'], memory)).toBe(0);
        expect(memory['arrayData'][0]).toEqual({
            id: array['id'],
            index: 0,
            startIndex: 0,
            endIndex: 10
        });
        expect(getArrayIndex(EMPTY_ARRAY_ID, memory)).toBe(1);
        expect(memory['arrayData'][1]).toEqual({
            id: EMPTY_ARRAY_ID,
            index: 1,
            startIndex: 10,
            endIndex: MEMORY_SIZE
        });
        expect(memory).toHaveProperty('freeSize', MEMORY_SIZE - 10);
    });

    describe('Resize array', () => {
        let array, secondArray;
        beforeEach(() => {
            array = memory.newArray(10);
            secondArray = memory.newArray(10);
            memory['memory'].fill(1, 0, 10);
            memory['memory'].fill(2, 10, 20);
        });

        test('Shrink', () => {
            memory.resizeArray(array, 8);

            expect(array).toHaveProperty('length', 8);
            expect(memory).toHaveProperty('freeSize', MEMORY_SIZE - 18);
            expect(memory['memory']).toEqual(new Int32Array(MEMORY_SIZE).fill(1, 0, 8).fill(2, 8, 18));
            expect(memory['arrayData'][0]).toEqual({
                id: array['id'],
                index: 0,
                startIndex: 0,
                endIndex: 8
            });
            expect(memory['arrayData'][1]).toEqual({
                id: secondArray['id'],
                index: 1,
                startIndex: 8,
                endIndex: 18
            });
        });

        test('Increase', () => {
            memory.resizeArray(array, 12);

            expect(array).toHaveProperty('length', 12);
            expect(memory).toHaveProperty('freeSize', MEMORY_SIZE - 22);
            expect(memory['memory']).toEqual(new Int32Array(MEMORY_SIZE).fill(1, 0, 10).fill(2, 12, 22));
            expect(memory['arrayData'][0]).toEqual({
                id: array['id'],
                index: 0,
                startIndex: 0,
                endIndex: 12
            });
            expect(memory['arrayData'][1]).toEqual({
                id: secondArray['id'],
                index: 1,
                startIndex: 12,
                endIndex: 22
            });
        });
    });

    test('Free array', () => {
        const array = memory.newArray(10);
        const secondArray = memory.newArray(10);
        memory['memory'].fill(1, 0, 10);
        memory['memory'].fill(2, 10, 20);

        memory.freeArray(array);

        expect(getArrayIndex(secondArray['id'], memory)).toBe(0);
        expect(memory).toHaveProperty('freeSize', MEMORY_SIZE - 10);
        expect(memory['memory']).toEqual(new Int32Array(MEMORY_SIZE).fill(2, 0, 10));
        expect(memory['data'][array['id']]).not.toBeDefined();
        expect(memory['arrayData'][0]).toEqual({
            id: secondArray['id'],
            index: 0,
            startIndex: 0,
            endIndex: 10
        });
    });
});

function getArrayIndex(id: string, memory: IMemoryManagement) {
    return memory['data'][id];
}
