import { v4 as uuid } from 'uuid';

import DynamicArray from './dynamic_array';

export const EMPTY_ARRAY_ID = 'empty_array_id';

export interface IMemoryManagement {
    newArray(size: number): number[] | null;
    resizeArray(array: number[], newSize: number): void;
    freeArray(array: number[]): void;
}

export interface IMemoryManagementArrayData {
    id: string;
    index: number;
    startIndex: number;
    endIndex: number;
}

export interface IMemoryManagementData {
    [key: string]: number;
}

export default class MemoryManagement implements IMemoryManagement {
    private memory: Int32Array; // stores the data
    private arrayData: IMemoryManagementArrayData[]; // stores information about arrays (indexes)
    private data: IMemoryManagementData; // stores information about ids and corresponding id in arrayData
    private freeSize: number;

    constructor(memorySize: number) {
        this.freeSize = memorySize;
        this.memory = new Int32Array(memorySize);
        this.arrayData = [];
        this.arrayData.push(
            { // always represents the free space and is always at the end of the memory array
                id: EMPTY_ARRAY_ID,
                index: 0,
                startIndex: 0, // only startIndex will be moved
                endIndex: memorySize // constant
            }
        );
        this.data = {
            [EMPTY_ARRAY_ID]: 0 // at the beginning the empty array is at 0 position
        };
    }

    newArray(size: number): number[] | null {
        if (!this.isThereEnoughMemory(size))
            return null;

        this.freeSize -= size;

        const emptyArray = this.emptyArray();
        const id = uuid();
        const newArray = {
            id,
            index: this.newArrayIndex(),
            startIndex: emptyArray.startIndex, // the new array gets the empty array start index because is put at the end
            endIndex: emptyArray.startIndex + size
        };

        // add new array to main arrays
        this.arrayData.splice(newArray.index, 0, newArray);
        this.data[id] = newArray.index;

        this.updateIndexes(newArray.index + 1, size); // update the next arrays indexes (in this case only the empty array)

        return new DynamicArray(id, this.memory, this.data, this.arrayData).create();
    }

    resizeArray(array: number[], newSize: number): void {
        // @ts-ignore
        const id = array['id'];
        const index = this.data[id];
        const data = this.arrayData[index];
        const offset = newSize - this.arrayLength(data); // how many elements will be added/removed

        if (this.freeSize < offset)
            return;

        this.freeSize -= offset;

        this.memory.copyWithin(data.endIndex + offset, data.endIndex); // move all elements after the array forward/backward (offset)
        if (offset > 0)
            this.memory.fill(0, data.endIndex, data.endIndex + offset); // if new elements are added they are filled with 0

        this.arrayData[index] = {
            id: id,
            index: index,
            startIndex: data.startIndex,
            endIndex: data.endIndex + offset // when resizing array only the endIndex is moved as the new/old elements are added/removed to/from end of the array
        };
        this.updateIndexes(index + 1, offset,false); // update the array indexes after the resized array with the specific offset without updating the indexes because no arrays are removed
    }

    freeArray(array: number[]): void {
        // @ts-ignore
        const id = array['id'];
        const index = this.data[id];
        const data = this.arrayData[index];
        const offset = this.arrayLength(data); // as the array is deleted, the offset is the full length

        this.freeSize += offset;

        this.memory.copyWithin(data.startIndex, data.endIndex);
        this.arrayData.splice(index, 1);
        delete this.data[id];

        this.updateIndexes(index, -offset); // will move start and end index on each array after removed one with the offset
    }

    private isThereEnoughMemory(size: number): boolean {
        return this.freeSize > size;
    }

    private emptyArray(): IMemoryManagementArrayData {
        return this.arrayData[this.arrayData.length - 1];
    }

    private newArrayIndex(): number {
        return this.arrayData.length - 1;
    }

    private updateIndexes(start: number, offset: number = 0, updateIndex: boolean = true) {
        for (let i = start; i < this.arrayData.length; i++) {
            const { id, index, startIndex, endIndex } = this.arrayData[i];
            this.arrayData[i] = {
                id,
                index: updateIndex ?
                    index + Math.sign(offset):
                    index,
                startIndex: startIndex + offset,
                endIndex: i != (this.arrayData.length - 1) ?
                    endIndex + offset :
                    endIndex
            };

            if (updateIndex)
                this.data[id] = this.arrayData[i].index;
        }
    }

    private arrayLength(data: IMemoryManagementArrayData): number {
        return data.endIndex - data.startIndex;
    }
}
