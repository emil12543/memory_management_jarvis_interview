import { IMemoryManagementArrayData, IMemoryManagementData } from './memory_management';

export interface IDynamicArray {
    create(): number[];
}

export default class DynamicArray implements IDynamicArray {
    constructor(
        private id: string, // identification for the memory data array
        private memory: Int32Array, // the main data array
        private data: IMemoryManagementData, // stores the array id and the index in the array data
        private arrayData: IMemoryManagementArrayData[] // stores the indexes
    ) {
    }

    public create(): number[] {
        return new Proxy<number[]>([], this.createHandler());
    }

    private createHandler(): ProxyHandler<number[]> {
        const self = this;
        return {
            get: (target, name) => {
                if (self.isDeleted())
                    return undefined;

                try {
                    if (!isNaN(Number(name))) {
                        const index = +<number>name;

                        return self.isIndexOutOfBounds(index) ?
                            undefined :
                            self.memory[self.getStartIndex() + index]; // gets the data from the main memory array
                    }
                } catch (e) { }

                switch (name) {
                    case 'id':
                        return self.id; // needed for the memory management operation (newArray, freeArray)
                    case 'length':
                        return self.getLength(); // needed to work like a real array
                    default:
                        return Reflect.get([], name);
                }
            },
            set: (target, prop, newValue) => {
                const index = +<number> prop;
                if (self.isIndexOutOfBounds(index) || self.isDeleted())
                    return false;

                this.memory[self.getStartIndex() + index] = newValue; // sets the data in the main memory array
                return true;
            },
            has(target, prop): boolean {
                if (self.isDeleted())
                    return false;

                return !self.isIndexOutOfBounds(+<number> prop);
            }
        }
    }

    private getStartIndex(): number {
        return this.arrayData[this.data[this.id]].startIndex;
    }

    private getLength(): number {
        return this.arrayData[this.data[this.id]].endIndex - this.getStartIndex();
    }

    private isIndexOutOfBounds(index: number): boolean {
        return index < 0 || index >= this.getLength();
    }

    private isDeleted() {
        return this.data[this.id] === undefined;
    }
}
